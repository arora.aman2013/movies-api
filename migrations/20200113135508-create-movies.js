'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('movies', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      title: {
        type: Sequelize.STRING(100)
      },
      description: {
        type: Sequelize.STRING(300)
      },
      runtime: {
        type: Sequelize.INTEGER(10)
      },
      genre: {
        type: Sequelize.STRING(50)
      },
      rating: {
        type: Sequelize.STRING(10)
      },
      metascore: {
        type: Sequelize.STRING(10)
      },
      votes: {
        type: Sequelize.INTEGER(10)
      },
      gross_earning_mil: {
        type: Sequelize.STRING(10)
      },
      director_id: {
        type: Sequelize.INTEGER(10),
        references: {
          model: 'directors',
          key: 'id'
        }
      },
      actor: {
        type: Sequelize.STRING(50)
      },
      year: {
        type: Sequelize.INTEGER(10)
      }
    },{
      timestamps: false
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('movies');
  }
};