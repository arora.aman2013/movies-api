'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('directors', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER(10)
      },
      name: {
        type: Sequelize.STRING(100)
      }
    }, {
      timestamps: false,
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('directors');
  }
};