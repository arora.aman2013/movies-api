const express = require('express');
const db = require('./models/index')
const bodyParser = require('body-parser')
// const directors = db.directors

const app = express()
// connection.querySelector();
app.use(bodyParser.json());
const port = 8000;


// for directors
app.get('/api/directors', function (req, res) {
    db.directors.findAll().then((result) => {
        console.log(result)
        res.status(200).send(result)
    }).catch((err) => {
        res.status(404).send({ data: {error: `can't retrieve data` }})
    })
});

app.post('/api/directors', function (req, res) {
    // console.log(req.body)
    db.directors.create({'name': req.body.name}).then((result => {
        // console.log('result', result.data.affectedRows);
        // if( result.data.affectedRows == 1){
            data = {'message': 'Director Added Successfully!'};
            res.status(201).send(data)
        // }        
        })).catch((err) => {
            res.status(404).send({ data: {error: `can't retrieve data` }})
        });
})

app.get('/api/directors/:id', function (req, res) {
    db.directors.findOne({where: {id: req.params.id}}).then((result => {
        res.status(200).send(result)
    })).catch(err => {
        res.status(404).send({data: {error: `can't retrieve data`}})
    })
})

app.delete('/api/directors/:id', function (req, res){
    db.directors.destroy({where: {id: req.params.id}}).then((result => {
        // if(result.data.affectedRows == 1){
            data = {'message': 'Director deleted successfully'}
            res.status(200).send(data)
        // }
    })).catch(err => {
        res.status(404).send({data: {error: `${error} in deleting director by id`}})
    })
})
app.put('/api/directors/:id', function (req, res){
    db.directors.update(req.body, {where: {id: req.params.id}}).then((result) => {
        // if(result.data.affectedRows == 1){
            data = {'message': 'Direcctor updated successfully'}
            res.status(200).send(data)
        // }
        }).catch((err) => {
            res.status(404).send({ data: {error: `can't retrieve data` }
        })
    });
})

// // for movies
app.get('/api/movies', function (req, res){
    db.movies.findAll().then((result) => {
        console.log(result)
        res.status(200).send(result)
    }).catch((err) => {
        res.status(404).send({ data: {error: `can't retrieve data` }})
    })
})
app.get('/api/movies/:id', function (req, res) {
    db.movies.findOne({where: {id: req.params.id}}).then((result => {
        if(result){
            res.status(200).send(result)
        }
        else{
            res.status(404).send('Not found')
        }
    })).catch(err => {
        res.status(404).send({data: {error: `can't retrieve data`}})
    })
})
app.delete('/api/movies/:id', function (req, res){
    db.movies.destroy({where: {id: req.params.id}}).then((result => {
        // if(result.data.affectedRows == 1){
            data = {'message': 'Movie deleted successfully'}
            res.status(200).send(data)
        // }
    })).catch(err => {
        res.status(404).send({data: {error: `${error} in deleting Movie by id`}})
    })
})
app.post('/api/movies', function (req, res) {
    console.log('from post', req.body);
    db.movies.create(req.body).then((result => {
        // console.log('result', result.data.affectedRows);
        // if( result.data.affectedRows == 1){
            data = {'message': 'Movie Added Successfully!'};
            res.status(201).send(data)
        // }        
        })).catch((err) => {
            res.status(404).send({ data: {error: `can't retrieve data` }})
        });
})
app.put('/api/movies/:id', function (req, res){
    db.movies.update(req.body, {where: {id: req.params.id}}).then((result) => {
        // if(result.data.affectedRows == 1){
            data = {'message': 'Movie updated successfully'}
            res.status(200).send(data)
        // }
        }).catch((err) => {
            res.status(404).send({ data: {error: `can't retrieve data` }
        })
    });
})

app.listen(port, () => {
    console.log('server created ', port);
})