'use strict';
const jsonToTables = require('../jsonToTables')
module.exports = {
  up: (queryInterface, Sequelize) => {
   return queryInterface.bulkInsert('movies', jsonToTables.movieDetails())
  },

  down: (queryInterface, Sequelize) => {
  //  return queryInterface.bulkDelete('movies', null, {});
  }
};