const express = require('express');
const connection = require('./connection')
const movies = require('./moviesFunction')
const directors = require('./directorFunction')
const bodyParser = require('body-parser')

const app = express()
// connection.querySelector();
app.use(bodyParser.json());
const port = 8000;


// for directors
app.get('/api/directors', function (req, res) {
    const list = directors.getAllDirectors();
    list.then((result) => {
       // console.log('resutl', result);
        res.status(200).send(result)
    }).catch((err) => {
        res.status(404).send({ data: {error: `can't retrieve data` }})
    });
});

app.post('/api/directors', function (req, res) {
    //console.log('from post', req.body);
    const list = directors.addDirector(req.body);
    list.then((result) => {
        console.log('resutl', result.data.affectedRows);
        if( result.data.affectedRows == 1){
            data = {'message': 'Director Added Successfully!'};
            res.status(200).send(data)
        }
        
    }).catch((err) => {
        res.status(404).send({ data: {error: `can't retrieve data` }})
    });
})

app.get('/api/directors/:id', function (req, res) {
    //console.log('from post', req.body);
    const list = directors.getDirectorById(req.params.id)
    list.then((result) => {
        res.status(200).send(result)
    }).catch((err) => {
        res.status(404).send({ data: {error: `can't retrieve data` }})
    });
})

app.delete('/api/directors/:id', function (req, res){
    const list = directors.deleteDirectorById(req.params.id)
    list.then((result) => {
        // console.log(result)
        if(result.data.affectedRows == 1){
            data = {'message': 'Direcctor deleted successfully'}
            res.status(200).send(data)
        }
    }).catch((err) => {
        res.status(404).send({data: {error: `can't delete data`}})
    })
})
app.put('/api/directors/:id', function (req, res){
    const list = directors.updateDirectorById(req.params.id, req.body['name'])
    list.then((result) => {
        if(result.data.affectedRows == 1){
            data = {'message': 'Direcctor updated successfully'}
            res.status(200).send(data)
        }
    }).catch((err) => {
        res.status(404).send({ data: {error: `can't retrieve data` }})
    });
})

// for movies
app.get('/api/movies', function (req, res){
    const list = movies.getAllMovies()
    list.then((result) => {
        res.status(200).send(result)
    }).catch((err) => {
        res.status(404).send({ data: {error: `can't retrieve data` }})
    });
})
app.get('/api/movies/:id', function (req, res) {
    // console.log('from post', req.params.id);
    const list = movies.getMovieById(req.params.id)
    list.then((result) => {
        res.status(200).send(result)
    }).catch((err) => {
        res.status(404).send({ data: {error: `can't retrieve data` }})
    });
})
app.delete('/api/movies/:id', function (req, res){
    // console.log(req.body)
    const list = movies.deleteMovieById(req.params.id)
    list.then((result) => {
        // console.log(result)
        if(result.data.affectedRows == 1){
            data = {'message': 'Direcctor deleted successfully'}
            res.status(200).send(data)
        }
    }).catch((err) => {
        res.status(404).send({data: {error: `can't delete data`}})
    })
})
app.post('/api/movies', function (req, res) {
    //console.log('from post', req.body);
    const list = movies.addMovie(req.body);
    list.then((result) => {
        console.log('resutl', result.data.affectedRows);
        if( result.data.affectedRows == 1){
            data = {'message': 'Director Added Successfully!'};
            res.status(200).send(data)
        }
        
    }).catch((err) => {
        res.status(404).send({ data: {error: `can't retrieve data` }})
    });
})
app.put('/api/movies/:id', function (req, res){
    const list = movies.updateMovieById(req.params.id, req.body)
    list.then((result) => {
        if(result.data.affectedRows == 1){
            data = {'message': 'Movie updated successfully'}
            res.status(200).send(data)
        }
    }).catch((err) => {
        res.status(404).send({ data: {error: `can't retrieve data` }})
    });
})

app.listen(port, () => {
    console.log('server created ', port);
})