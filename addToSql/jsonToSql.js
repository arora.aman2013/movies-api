const mysql = require('mysql');
const movieJson = require('./movies.json')

const connection = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: '1234',
    database: 'moviesDirector'
})

const createDirector = 'create table director(id int primary key auto_increment, name varchar(50))';

const insertQueryDirector = 'insert into director (name) values'

let directorName = [];

// console.log(movieJson);

for (movie of movieJson) {
    if (directorName.indexOf(movie['Director']) === -1) {
        directorName.push(movie['Director'])
    }
}

// console.log(directorName)

const createMovie = 'CREATE TABLE movie(id  INT PRIMARY KEY AUTO_INCREMENT, title VARCHAR(100), description VARCHAR(300), runtime INT, genre varchar(50), rating FLOAT, metascore varchar(10), votes INT, gross_earning_mil varchar(20), director_id INT, FOREIGN KEY (`director_id`) REFERENCES `director`(`id`), actor VARCHAR(50), year YEAR(4));';

const insertQueryMovie = 'INSERT INTO movie (title, description, runtime, genre, rating, metascore, votes, gross_earning_mil, director_id, actor, year) VALUES ?';
const movies = [];

for(movie of movieJson){
    let singleMovie = []
    for(movieDeatail in movie){
        if(movieDeatail != 'Director'){
            singleMovie.push(movie[movieDeatail])
        }
        else{
            singleMovie.push(directorName.indexOf(movie[movieDeatail])+1);
            // console.log(directorName.indexOf(movie[movieDeatail])+1)
        }
    }
    movies.push(singleMovie)
}
// console.log(movies)

connection.connect((err) => {
    if (err) throw err;
    else {
        console.log('connected')
        connection.query(createDirector, (err, result) => {
            if(err) throw err;
            console.log(result);
        })
        for (let name of directorName) {
            connection.query(insertQueryDirector + ` ("${name}")`, (err, result) => {
                if (err) throw err;
                console.log(result);
            })
        }
        connection.query(createMovie, (err, result) => {
            if (err) throw err;
            console.log(result);
        });
        connection.query(insertQueryMovie, [movies], (err, result) => {
            if (err) throw err;
            console.log(result);
        });
        connection.end();
    }
})