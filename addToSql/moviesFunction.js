const connection = require('./connection')

function getAllMovies(){
    const getAllMoviesQuery = 'select * from movie;';
    return connection.querySelector(getAllMoviesQuery)
}

function getMovieById(id){
    const getMovieByIdQuery = `select * from movie where id = ${id};`
    return connection.querySelector(getMovieByIdQuery);
}

function deleteMovieById(id){
    const deleteMovieByIdQuery = `delete from movie where id = ${id};`
    return connection.querySelector(deleteMovieByIdQuery);
}

function addMovie(movieDetails){
    const addMovieQuery = `insert into movie (title, description, runtime, genre, rating, metascore, votes, gross_earning_mil, director_id, actor, year) VALUES ("${movieDetails['title']}", "${movieDetails['description']}", ${movieDetails['runtime']}, "${movieDetails['genre']}", ${movieDetails['rating']}, "${movieDetails['metascore']}", ${movieDetails['votes']}, "${movieDetails['gross_earning_mil']}", ${movieDetails['director_id']}, "${movieDetails['actor']}", ${movieDetails['year']});`
    return connection.querySelector(addMovieQuery);
}

function updateMovieById(id, movieDetails){
    let updateMovieByIdQuery = `update movie set `
    for(movie in movieDetails){
        console.log(movieDetails[movie])
        updateMovieByIdQuery += `${movie} = ${movieDetails[movie]} `
    }
    updateMovieByIdQuery += `where id = ${id};`
    // console.log(updateMovieByIdQuery)
    return connection.querySelector(updateMovieByIdQuery);
}


// getMovieById('2');
// let x = {
//     "id": 1,
//     "title": "The Shawshank Redemption",
//     "description": "Two imprisoned men bond over a number of years, finding solace and eventual redemption through acts of common decency.",
//     "runtime": 142,
//     "genre": "Crime",
//     "rating": 9.3,
//     "metascore": 80,
//     "votes": 1934970,
//     "gross_earning_mil": 28.34,
//     "director_id": "1",
//     "actor": "Tim Robbins",
//     "year": 1994
// }
// updateMovieById('1', x);
// addMovie(x);
// deleteMovieById('51')

module.exports = {
    getAllMovies,
    getMovieById,
    deleteMovieById,
    addMovie,
    updateMovieById
}