const connection = require('./connection')

async function getAllDirectors(){
    const getAllDirectorsQuery = 'select * from director;';
    const dataJson =  await connection.querySelector(getAllDirectorsQuery);
    const data = JSON.parse(JSON.stringify(dataJson.data))
    return data;
}

async function getDirectorById(id){
    const getDirectorByIdQuery = `select * from director where id = ${id};`
    const dataJson =  await connection.querySelector(getDirectorByIdQuery);
    const data = JSON.parse(JSON.stringify(dataJson.data))
    return data;
}

async function deleteDirectorById(id){
    const deleteDirectorByIdQuery = `delete from director where id = ${id};`
    const data = await connection.querySelector(deleteDirectorByIdQuery);
    return data;
}

async function addDirector(bodyData){
    //console.log('add:',  bodyData.name);
    const addDirectorQuery = `insert into director (name) values ("${bodyData['name']}")`;
    const data = await connection.querySelector(addDirectorQuery);
    console.log(data);
    return data;
}

async function updateDirectorById(id, directorName){
    const updateDirectorByIdQuery = `update director set name = "${directorName}" where id = ${id};`
    const data = await connection.querySelector(updateDirectorByIdQuery);
    return data;
}

// updateDirectorById('1', 'Frank Darabont');
// getDirectorById('1')
// getAllDirectors()
// addDirector('Aman');
// deleteDirectorById('36')

module.exports = {
    getAllDirectors,
    getDirectorById,
    deleteDirectorById,
    addDirector,
    updateDirectorById
}