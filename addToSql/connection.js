const mysql = require('mysql');

const connection = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: '1234',
    database: 'moviesDirector'
})

connection.connect((err) => {
    if (err) throw err;
    console.log('Connected!');
});

function querySelector(query){
    return new Promise((resolve, reject) => {
        connection.query(query, (err, result) => {
            if (err){
                throw err;
            }
            else{
                const resultJSON = {data: result};
                // console.log(resultJSON)
                resolve(resultJSON)
               
            }
        })
    })
}

module.exports = {
    connection,
    querySelector
}