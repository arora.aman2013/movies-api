$ mkdir sequelize-demo
$ cd sequelize-demo
$ npm init

npm install sequelize-cli
$ npm install sequelize --save
$ sequelize init

npm install mysql2 --save

give database name and pass in config.json

Create models for table 
sequelize model:create --name User --attributes first_name:string,last_name:string,bio:text
Change the migration files according to table details

sequelize db:migrate
to add tables to the database

write function to add data to tables
create seed
sequelize seed:create --name my-seed-file

seed data now
sequelize db:seed:all
data is added to table now

now create express routes
npm install express
findAl- To get all users
findOne-  to get one
destroy- to delete
create- To add in table
update- to update contents of table