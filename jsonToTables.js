const movieJson = require('./movies.json')

let directorName = [];
let name = []

// console.log(movieJson);
function dir(){
    for (movie of movieJson) {
        if (name.indexOf(movie['Director']) === -1) {
            name.push(movie['Director'])
        }
    }
    for(let i=0; i<name.length; i++){
        let director = {}
        // director.id = i+1;
        director.name = name[i];
        directorName.push(director)
    }
    // console.log(directorName)
    return directorName;
}
// dir()

function movieDetails(){
    dir()
    const movies = [];
    for(movie of movieJson){
        let singleMovie = {}
        for(movieDeatail in movie){
            if(movieDeatail === 'Director'){
                // singleMovie.push(directorName.indexOf(movie[movieDeatail])+1);
                singleMovie.director_id = name.indexOf(movie[movieDeatail])+1
            }
            if(movieDeatail === 'Rank'){
                // console.log('1')
                continue
            }
            if (movieDeatail === 'Title'){
                singleMovie.title = movie[movieDeatail]
            }
            if(movieDeatail === 'Description'){
                singleMovie.description = movie[movieDeatail]
            }
            if(movieDeatail === 'Runtime'){
                singleMovie.runtime = movie[movieDeatail]
            }
            if(movieDeatail === 'Genre'){
                singleMovie.genre = movie[movieDeatail]
            }
            if(movieDeatail === 'Rating'){
                singleMovie.rating = movie[movieDeatail]
            }
            if(movieDeatail === 'Metascore'){
                singleMovie.metascore = movie[movieDeatail]
            }
            if(movieDeatail === 'Votes'){
                singleMovie.votes = movie[movieDeatail]
            }
            if(movieDeatail === 'Gross_Earning_in_Mil'){
                singleMovie.gross_earning_mil = movie[movieDeatail]
            }
            if(movieDeatail === 'Actor'){
                singleMovie.actor = movie[movieDeatail]
            }
            if(movieDeatail === 'Year'){
                singleMovie.year = movie[movieDeatail]
            }
            // singleMovie.push(movie[movieDeatail])
            // console.log(directorName.indexOf(movie[movieDeatail])+1)
        }
        movies.push(singleMovie)
    }
    // console.log(movies)
    return movies
}
// movieDetails()
module.exports = {
    dir,
    movieDetails
}